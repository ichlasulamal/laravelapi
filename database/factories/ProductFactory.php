<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->safeEmail(),
            'category_id' => $this->faker->randomDigitNotNull(),
            'detail_id' => $this->faker->randomDigitNotNull(),


            
        ];
    }
}
