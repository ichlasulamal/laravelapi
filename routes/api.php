<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Product;


use App\Models\Category;
use App\Models\Detail;
use App\Http\Controllers\API\ProductController;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Facades\DB;





// Soal 2 Nomor 1 return respon data product paginated
// Route::get('/products', function(){
//     $products = \App\Models\Product::paginate(3);
//     return $products;
// });

Route::get('/products',[ProductController::class, 'index']); #Style MVC

// Untuk akses soal nomor 2 harus register user dulu, kemudian login dapat token, baru bisa menggunkan token pada saat get data agar tidak error
// Protect Api dengan Sanctum
// Soal 2 nomor 2 return respon product, category & product detailnya
Route::middleware('auth:sanctum')->get('/detail_products', function(){

    $products = DB::table('products')
                    ->join('categories', 'products.category_id', '=', 'categories.id')
                    ->join('details', 'products.detail_id', '=', 'details.id')

                    ->select('products.id as id', 'products.name as product', 'categories.name as category', 'details.desc as detail_desc')
                    ->get();
                    
    return response()->json(['data' => $products]);
});

// Soal 2 nomor 3 return respon data product with sort ordering parameter
Route::get('/order_product/{sort?}', function($sort='id'){
    $products = Product::orderByDesc($sort)->get();

    return response()->json(['data' => $products]);
});

// Soal 2 Nomor 4 respon product & group by category
Route::get('/categories_model/{id?}', function($id=1){
    $categories = Category::find(1)->where('id',$id)->get();
    $output = $categories[0]->products;

    return response()->json(['data' => $output ]);;
})->where('id', '[0-9]+');





/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//API route for register new user
Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
//API route for login user
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);

//Protecting Routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function(Request $request) {
        return auth()->user();
    });

    // API route for logout user
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
