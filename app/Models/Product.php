<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use \App\Models\Category;
use \App\Models\Detail;


class Product extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
    public function detail()
    {
        return $this->hasOne(Detail::class, 'id', 'detail_id');
    }
   

}
